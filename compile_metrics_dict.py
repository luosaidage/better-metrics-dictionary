import yaml
from pathlib import Path
import json
import gitlab
import argparse
import os

def get_metric_files(project, file_or_folder, files):
    if file_or_folder["type"] == "tree":
        folder = project.repository_tree(path=file_or_folder["path"], as_list=False)
        for folder_path in folder:
            get_metric_files(project,folder_path, files)
    elif file_or_folder["type"] == "blob":
        files.append(file_or_folder)

def download_file(project, file):
    try:
        file_content = project.repository_raw_blob(file["id"])
    except Exception as e:
        print("Could not download file: %s" % file["path"])
        print(e)
        return
    if not os.path.exists(os.path.dirname(file["path"])):
        os.makedirs(os.path.dirname(file["path"]))
    with open(file["path"],"wb") as file:
        file.write(file_content)

gl = gitlab.Gitlab("https://gitlab.com")

crawl = True

if crawl:
    gitlab_project = gl.projects.get(278964)
    metrics_folder = gitlab_project.repository_tree(path='config/metrics', as_list=False)
    files = []
    for file_or_folder in metrics_folder:
        get_metric_files(gitlab_project, file_or_folder, files)

    for file in files:
        download_file(gitlab_project, file)

    metrics_folder = gitlab_project.repository_tree(path='ee/config/metrics', as_list=False)
    files = []
    for file_or_folder in metrics_folder:
        get_metric_files(gitlab_project, file_or_folder, files)

    for file in files:
        download_file(gitlab_project, file)

metric_def_list = list(Path("config/metrics/").rglob("*.yml"))

metric_dict = {}
for metric_def_file_path in metric_def_list:
    with open(metric_def_file_path,"r") as metric_def_file:
        # deal with these later
        if "aggregates" in str(metric_def_file_path):
            continue
        metric_def = yaml.safe_load(metric_def_file)
        if metric_def["key_path"] not in metric_dict:
            metric_dict[metric_def["key_path"]] = metric_def
        else:
            print("Warn: Metric def already exists: %s" % metric_def_file_path)

metric_def_list = list(Path("ee/config/metrics/").rglob("*.yml"))

for metric_def_file_path in metric_def_list:
    with open(metric_def_file_path,"r") as metric_def_file:
        # deal with these later
        if "aggregates" in str(metric_def_file_path):
            continue
        metric_def = yaml.safe_load(metric_def_file)
        if metric_def["key_path"] not in metric_dict:
            metric_dict[metric_def["key_path"]] = metric_def
        else:
            print("Warn: Metric def already exists: %s" % metric_def_file_path)

with open("metrics.json","w") as outfile:
    json.dump(metric_dict, outfile, indent=2)