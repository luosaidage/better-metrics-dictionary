import json
from mako.template import Template

metrics = {}
with open("metrics.json","r") as metrics:
    metrics = json.load(metrics)

metrics_list = list(metrics.values())

for metric in metrics_list:
    metric["tier"] = ", ".join(metric["tier"])
    if metric["tier"] == "premium, ultimate":
        metric["tier"] = "premium"
    elif "free" in metric["tier"]:
        metric["tier"] = "free"
    elif metric["tier"] == "ultimate":
        metric["tier"] = "ultimate"
    if "performance_indicator_type" in metric:
        metric["performance_indicator_type"] = ", ".join(metric["performance_indicator_type"])

mytemplate = Template(filename='template/index.html')

fields = ["key_path","data_category","description","product_section","product_stage","product_group","product_category","value_type","status","time_frame","tier","performance_indicator_type","milestone"]

with open("public/index.html","w") as outfile:
    outfile.write(mytemplate.render(metrics = metrics_list, fields = fields))